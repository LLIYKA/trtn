﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DisplayedDevices
{
    class Program
    {
        static void Main(string[] args)
        {
            Watcher.visulize += new Action<List<device>>((ListOfDevices) =>

             {
                 Console.Clear();
                 Console.WriteLine("Для выхода введите Q");
                 Console.WriteLine("Для изменения порогов введите через пробел номер_устройства значение1 значение2");
                 foreach (var itemDevice in ListOfDevices)
                 {
                     Console.WriteLine("Устройство: " + itemDevice.numbDevice);
                     Console.WriteLine("Параметр 1: " + itemDevice.firstVal);
                     Console.ForegroundColor = (itemDevice.secondVal > itemDevice.maxVal || itemDevice.secondVal < itemDevice.minVal ? ConsoleColor.Red : ConsoleColor.White);
                     Console.WriteLine("Параметр 2: " + itemDevice.secondVal);
                     Console.ForegroundColor = ConsoleColor.White;
                     Console.WriteLine("Макс порог: " + itemDevice.maxVal);
                     Console.WriteLine("Мин порог: " + itemDevice.minVal);
                     Console.WriteLine("---------------------------------");
                 }
             });
            Watcher.run();
            var userCommand = "";
            do
            {
                userCommand = Console.ReadLine();
                var arrayArg = userCommand.Split(' ');
                if (userCommand != "Q" && arrayArg.Length == 3)
                {
                    Watcher.SendLW(arrayArg);
                }
            }
            while (userCommand != "Q");
            Watcher.close();
        }
    }
}
