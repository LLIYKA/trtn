﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DisplayedDevices
{

    public static class Watcher
    {
        static List<device> ListOfDevices;
        static string remoteAddress = "127.0.0.1";
        static int remotePort = 62005;
        public static event Action<List<device>> visulize;
        public static bool Working = false;
        static UdpClient client = new UdpClient(62006);
        static UdpClient ForSend;
        static Encoding ascii = Encoding.ASCII;
        public static void run()
        {

            
            ForSend = new UdpClient();
            ListOfDevices = new List<device>();

            Thread tupdateInfo = new Thread(new ThreadStart(() =>
            {
                byte[] data;
                while (Working)
                {
                    IPEndPoint ip = null;
                    data = client.Receive(ref ip);
                    var deviceNum = BitConverter.ToInt32(data, 0);
                    var command = BitConverter.ToUInt16(data, 4);


                    var tryingDevice = ListOfDevices.Where(x => x.numbDevice == deviceNum).FirstOrDefault();
                    if (tryingDevice == null)
                    {
                        tryingDevice = new device();
                        ListOfDevices.Add(tryingDevice);
                    }

                    if (command <= 100)
                    {
                        tryingDevice.numbDevice = BitConverter.ToInt32(data, 0);
                        tryingDevice.firstVal = BitConverter.ToUInt16(data, 4);
                        tryingDevice.secondVal = BitConverter.ToUInt16(data, 6);

                        var getLimitedOf = new List<byte>();
                        getLimitedOf.AddRange(BitConverter.GetBytes(tryingDevice.numbDevice));
                        getLimitedOf.AddRange(ascii.GetBytes("LR"));
                        ForSend.Send(getLimitedOf.ToArray(), getLimitedOf.Count, remoteAddress, remotePort);
                    }
                    else if (command > 100)
                    {
                        var status = BitConverter.ToUInt16(data, 6);
                        if (status == 0)
                            tryingDevice.maxVal = BitConverter.ToUInt16(data, 8);
                        tryingDevice.minVal = BitConverter.ToUInt16(data, 10);

                        if (visulize != null)
                            visulize.Invoke(ListOfDevices);
                    }
                }
            }));
            tupdateInfo.Start();
        }
        public static void SendLW(string[] arrayArg)
        {
            var getLimitedOf = new List<byte>();
            getLimitedOf.AddRange(BitConverter.GetBytes(int.Parse(arrayArg[0])));
            getLimitedOf.AddRange(ascii.GetBytes("LW"));
            getLimitedOf.AddRange(BitConverter.GetBytes(Int16.Parse(arrayArg[1])));
            getLimitedOf.AddRange(BitConverter.GetBytes(Int16.Parse(arrayArg[2])));
            ForSend.Send(getLimitedOf.ToArray(), getLimitedOf.Count, remoteAddress, remotePort);
        }
        public static void close()
        {
            Working = false;
            client.Close();
            ForSend.Close();
        }
    }
}
