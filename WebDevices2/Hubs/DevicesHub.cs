﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisplayedDevices;
using Microsoft.AspNet.SignalR;

namespace WebDevices2.Hubs
{
    public class DevicesHub : Hub
    {

        public DevicesHub()
        {
            if (!Watcher.Working)
            {
                Watcher.Working = true;
                Watcher.visulize += new Action<List<device>>((ListOfDevices) =>
                {
                    Clients.All.addMessage(ListOfDevices);

                });
                Watcher.run();
            }
            //Watcher.close();
        }
        public void SendLW(string[] SetData)
        {
            if (SetData.Length == 3)
                Watcher.SendLW(SetData);
        }
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }
    }
}