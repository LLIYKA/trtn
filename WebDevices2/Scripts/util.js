﻿$(function () {
    
    var viser = $.connection.devicesHub;
    
    viser.client.addMessage = function (ListOfDevices) {

        for (i = 0; i < ListOfDevices.length; i++) {
            if ($('.devicesListView').find('.containerDevice' + ListOfDevices[i].numbDevice).length !== 0)
                updateInfo(ListOfDevices[i]);
            else {
                $('.devicesListView').append(wrapDevice(ListOfDevices[i]));
                $('.setLimits').last().on('click', function () {
                    viser.server.sendLW([$(this).attr('idDev'), $(this).parent().find('.setDevMax').val(), $(this).parent().find('.setDevMin').val()]);
                });
            }
        }

        
    };
    function wrapDevice(itemDevice) {
        return '<p class="containerDevice' + itemDevice.numbDevice + '"><b>Устройство: ' + itemDevice.numbDevice + '</b>' +
            '<br> Параметр 1: <span class="val1Dev">' + itemDevice.firstVal + '</span>' +
            '<br> Параметр 2: <span class="val2Dev">' +
            (itemDevice.secondVal > itemDevice.maxVal || itemDevice.secondVal < itemDevice.minVal ?
                '<span style="color:red;">' + itemDevice.secondVal + '</span>'
                :
                itemDevice.secondVal
            ) + ' </span>' +
            '<br> Макс порог: <span class="limMaxDev">' + itemDevice.maxVal + '</span>  <input size="3" type="text" class="setDevMax">' +
            '<br> Мин порог: <span class="limMinDev">' + itemDevice.minVal + '</span> &nbsp; &nbsp;<input size="3" type="text" class="setDevMin"> ' +
            '<input idDev="' + itemDevice.numbDevice + '" class="setLimits" type="button" value="Установить">' + '</p>';
    }
    function updateInfo(itemDevice) {

        var itemContainer = $('.devicesListView').find('.containerDevice' + itemDevice.numbDevice);
        itemContainer.find('.val1Dev').html(itemDevice.firstVal);
        itemContainer.find('.val2Dev').html(
            (itemDevice.secondVal > itemDevice.maxVal || itemDevice.secondVal < itemDevice.minVal ?
                '<span style="color:red;">' + itemDevice.secondVal + '</span>'
                :
                itemDevice.secondVal
            ));
        itemContainer.find('.limMaxDev').html(itemDevice.maxVal);
        itemContainer.find('.limMinDev').html(itemDevice.minVal);

    }

    $.connection.hub.start().done(function () {
       
    });

});
