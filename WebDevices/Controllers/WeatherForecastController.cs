﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Web;


namespace WebDevices.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        // Has to be called something starting with "Get"
        public HttpResponseMessage Get()
        {
            HttpContext.Current.AcceptWebSocketRequest(new SomeWebSocketHandler());
            return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
        }

        class SomeWebSocketHandler : WebSocketHandler
        {
            public SomeWebSocketHandler() { SetupNotifier(); }
            protected void SetupNotifier()
            {
                // Call a method to handle whichever change you want to broadcast
                var messageToBroadcast = "Hello world";
                broadcast(messageToBroadcast);
            }

            private static WebSocketCollection _someClients = new WebSocketCollection();

            public override void OnOpen()
            {
                _someClients.Add(this);
            }

            public override void OnMessage(string message)
            {

            }

            private void broadcast(string message)
            {
                _someClients.Broadcast(msg);
                SetupNotifier();
            }
        }

    }
}
